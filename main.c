#include <stdio.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>

#define IFACE_ARG_POS 1
#define ADDR_ARG_POS 2
#define PORT_ARG_POS 3

static int terminate_flag = 0;

int iface_up(int sd) 
{
    struct ifreq ifr;
    int err = ioctl(sd, SIOCGIFFLAGS, &ifr);
    if (err < 0)
        return err;
    ifr.ifr_flags |= (IFF_UP | IFF_RUNNING);
    err = ioctl(sd, SIOCSIFFLAGS, &ifr);
    return err;
}

int iface_down(int sd)
{
    struct ifreq ifr;
    int err = ioctl(sd, SIOCGIFFLAGS, &ifr);
    if (err < 0)
        return err;
    ifr.ifr_flags &= ~(IFF_UP | IFF_RUNNING);
    err = ioctl(sd, SIOCSIFFLAGS, &ifr);
    return err;
}

int iface_set_addr(int sd, char* iface_name, char* address)
{
    struct ifreq ifr;
    strncpy(ifr.ifr_name, iface_name, IFNAMSIZ);
    struct sockaddr_in* addr = (struct sockaddr_in*)&ifr.ifr_addr;
    addr->sin_family = AF_INET;
    inet_pton(AF_INET, address, &addr->sin_addr);
    return ioctl(sd, SIOCSIFADDR, &ifr);    
}

void handle_signal(int signum) { terminate_flag = 1; }

void handle_connection(int socket)
{
    if (fork() == 0) {
        for (;;) {
            char buf[1024];
            ssize_t n = read(socket, buf, 1024);
            if (n < 0) {
                strcpy(buf, "RECIEVE TIMEOUT");
                write(socket, buf, strlen(buf));
                close(socket);
                exit(0);
            }
            write(socket, buf, n);
        }
    }    
}

int main(int argc, char **argv)
{
    if (argc < 4) {
	    printf("Usage: %s INTERFACE ADDRESS PORT\n\r", argv[0]);
        return 0;
    }

    signal(SIGINT, handle_signal);

    char *iface_name = argv[IFACE_ARG_POS];

    int server_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
    if (server_socket < 0) {
	    perror("Unable to create socket");
        return -1;
    }

    printf("Try to set iface %s address to %s\r\n", argv[IFACE_ARG_POS], argv[ADDR_ARG_POS]);
    int err = iface_set_addr(server_socket, argv[IFACE_ARG_POS], argv[ADDR_ARG_POS]);
    if (err < 0) {
        perror("Unable to configure iface...\r\n");
        return -1;
    }

    printf("Try to up iface %s\r\n", argv[IFACE_ARG_POS]);
    err = iface_up(server_socket);
    if (err < 0) {
        perror("Unable to configure iface...\r\n");
        return -1;
    }

    short int port = strtol(argv[PORT_ARG_POS], NULL, 10);
    struct sockaddr_in server_address;
    memset(&server_address, 0, sizeof(server_address));

    inet_pton(AF_INET, argv[ADDR_ARG_POS], &server_address);
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(port);
    server_address.sin_addr.s_addr = INADDR_ANY;

    struct timeval tv;
    tv.tv_sec = 5;
    tv.tv_usec = 0;
    err = setsockopt(server_socket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
    if (err < 0) {
        perror("Unable to configure socket receive timeout\r\n");
        return -1;        
    }

    err = bind(server_socket, (struct sockaddr *)&server_address, sizeof(struct sockaddr));
    if (err < 0) {
        perror("Unable to configure server socket\r\n");
        return -1;
    }

    listen(server_socket, 1);
    
    for (;terminate_flag == 0;) {
        struct sockaddr_in peer_saddr;
        socklen_t peer_len = sizeof(struct sockaddr);
        memset(&peer_saddr, 0, sizeof(peer_saddr));
        int client = accept(server_socket, (struct sockaddr *)&peer_saddr, &peer_len);
        if (client >= 0) {
            printf("Incoming connection\r\n");
            handle_connection(client);
        }
        close(client);
    }

    printf("Try to down iface %s\r\n", argv[IFACE_ARG_POS]);
    err = iface_down(server_socket);
    if (err < 0) {
        perror("Unable to down iface\r\n");
        return -1;   
    }
    signal(SIGINT, SIG_DFL);
    return 0;
}